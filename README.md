# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.
-------------

Author: Megan Walter

Contact Address: mwalter2@uoregon.edu

Description: Contains a simple program (/hello/hello.py) that prints the message, "Hello world".



